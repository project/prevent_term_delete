Prevent Term Delete

 This module is built to prevent the taxonomy term deletion when the taxonomy term is associated with entites using taxonomy term reference. 

 Features

  This feature can  be enabled or disabled  for particular vocabulary using config form.
  Admin can limit the number of records to show on term delete form.
  Admin can disable or enable delete button in term delete form, when disabled admin cannot be able to 
  delete the term as it is associated with the entities, when enabled admin or user can delete even though when the term is associated with the entities 


